This repo contains all the files needed to generate a static copy of your WordPress site.

Read the [blog post](http://craftcodecrew.com/wordpress-vs-static-web-pages-the-best-of-both-worlds/) at craftcodecrew.com for more information.

You should place all the files in this directory (except for the .gitlab-ci.yml) into the root of your project.

You can access the static web page at yourdomain.tld/ and your wp installation at yourdomain.tld/wp/.

## .htpasswd

This file stores usernames and passwords to access your WordPress installation.

The demo file in this repository contains a user called "user" with password "craftcoders".

**Please replace this file with your own!**

You can recreate the .htpasswd file in \*NIX with:

'' rm .htpasswd && htpasswd -c .htpasswd yourusername ''

Or use an online generator (at your own risk).

## .htaccess

This file contains the server config to redirect visitors to /static and to protect
your word press installation with the username and passwords defined in .htpasswd.

**You need to adjust the path to your .htpasswd file in here.**

## admin-ajax-oAEhFc.php

This is the alternative implementation of admin-ajax.php to allow ninja forms.

## wp_2789218/

Install wordpress into this directory.

You can rename this directory to something else but then you will also need
to replace wp_2789218 in the .htaccess, .gitlab-ci.yml and admin-ajax-oAEhFc.php files. 

## static/

This is where the static copy of your website goes.

## Contact information

Sebastian Gellweiler

gellweiler@craft-coders.de

DevOps engineer at [CRAFTCODERS](https://craft-coders.de)

Karlsruhe, Germany
